A CI / CD test using Gitlab Pipelines, Docusaurus (React), Snyk CLI, Surge.sh and AWS S3 by Evandro Morini

Develop Environment: http://develop-devops-evandro-morini.surge.sh

Production Environment: http://devops-evandro-morini.s3-website-us-east-1.amazonaws.com/